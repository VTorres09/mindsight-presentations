from datetime import datetime, timedelta
import numpy as np

import bentoml
from bentoml.io import JSON

iris_clf_runner = bentoml.sklearn.get("iris_clf:latest").to_runner()

svc = bentoml.Service("iris_classifier", runners=[iris_clf_runner])


def group_extra_hours_by_month(last_six_months_extra_hours):
    months_group = {}

    for extra_hour in last_six_months_extra_hours:
        year_month = extra_hour["date"].split(
            "-")[0] + "-" + extra_hour["date"].split("-")[1]

        if year_month not in months_group:
            months_group[year_month] = 0

        months_group[year_month] += extra_hour["hours"]

    return months_group


def calculate_extra_hour_average_last_four_months(months_group):
    now = datetime.now()

    last_four_months_average = []

    for i in range(1, 5):

        month = now.month - i
        year = now.year

        if month < 1:
            month += 12
            year -= 1

        if f"{year}-{month:02d}" in months_group:
            last_four_months_average.append(
                months_group[f"{year}-{month:02d}"])
        else:
            last_four_months_average.append(0)

    return last_four_months_average


def extra_hour_alert(person):
    '''
        Calcular a média e desvio padrão dos últimos 4 meses. 

        IF(
            AND(
                média horas extras <= 5;
                desvio horas extras <= 5
            );
            "Sem alerta"; 
            IF(
                AND(média horas extras <= 10;desvio horas extras <= 5);
                "Constante necessidade de horas extras"; 
                IF(
                    AND(
                        média horas extras <= 10;
                        desvio horas extras >5
                    );
                    "Alta carga periódica de horas extras";
                    "Constante alta carga de horas extras"
                )
            )
        )

    '''
    average_extra_hours_last_four_months = calculate_extra_hour_average_last_four_months(
        group_extra_hours_by_month(person["last_six_months_extra_hours"]))

    average_extra_hours = np.average(average_extra_hours_last_four_months)

    standard_deviation_extra_hours = np.std(
        average_extra_hours_last_four_months)

    if average_extra_hours <= 5 and standard_deviation_extra_hours <= 5:
        return "Sem alerta"
    elif average_extra_hours <= 10 and standard_deviation_extra_hours <= 5:
        return "Constante necessidade de horas extras"
    elif average_extra_hours <= 10 and standard_deviation_extra_hours > 5:
        return "Alta carga periódica de horas extras"
    else:
        return "Constante alta carga de horas extras"


def raise_alert(person):
    '''
    Primeiro, dividir o número de anos que a pessoa tem na empresa pela quantidade de aumentos por mérito ao aumento. 

    IF(
        AND(
            média de aumentos por mérito<=0.5;
            última AVD = "Outstanding Performance";
            data do último aumento < (HOJE()-1095)
        );
        "Crítico";
            IF(
                AND(
                    média de aumentos pro mérito <=0.5;
                    OU(
                        AJ2="Strong Performance";
                        AJ2="Outstanding Performance"
                    );
                    data último aumento < (HOJE()-720);
                    Q2<(HOJE()-720)
                );
                "Moderado";
                Sem alerta
            )
        )
    '''

    last_raise_date = datetime.strptime(
        person["last_raise_date_without_adjusts"], '%Y-%m-%d')

    raises_average = person["number_of_raises_without_adjusts"] / \
        (person["company_time_in_days"] / 365)

    if raises_average <= 0.5 and \
            person["evaluation"]["score_description"] == "Outstanding Performance" and \
            last_raise_date < (datetime.now() - timedelta(days=1095)):

        return "Crítico"

    elif raises_average <= 0.5 and \
        (person["evaluation"]["score_description"] == "Strong Performance" or person["evaluation"]["score_description"] == "Outstanding Performance") and \
            last_raise_date < (datetime.now() - timedelta(days=720)):
        datetime.strptime(person["last_raise_date_without_adjusts"] < (
            datetime.now() - timedelta(days=720)))

        return "Moderado"

    else:

        return "Sem alerta"


def turnover_alert(person, raise_alert_result):
    '''
    IF(
        AND(
            OR(
                Delta clima fator 2 - company_average <0;
                Delta clima fator 3 - company_average <0
            );
            quantidade de aumentos por mérito = 0; 
            possui qualquer alerta aumento 
        );
        "Crítico";
        IF(
            OR(
                Delta clima fator 2 - company_average < 0;
                Delta clima fator 3 - company_average < 0;
            );
            "Moderado";
            "Não crítico"
        )
    )
    '''
    if len(person["manager_climate_factors"]) > 0:
        climate_factor_2 = {}
        climate_factor_3 = {}

        for climate_factor in person["manager_climate_factors"]:
            if climate_factor["factor_name"] == "Categoria #2":
                climate_factor_2 = climate_factor

            if climate_factor["factor_name"] == "Categoria #3":
                climate_factor_3 = climate_factor

        delta_clima_condition = (climate_factor_2["score"] - climate_factor_2["company_average"]) or (
            climate_factor_3["score"] - climate_factor_3["company_average"])
    else:
        delta_clima_condition = False

    if delta_clima_condition and \
        person["number_of_raises_without_adjusts"] == 0 and \
            (raise_alert_result == "Crítico" or raise_alert_result == "Moderado"):

        return "Crítico"

    elif delta_clima_condition:

        return "Moderado"

    return "Sem alerta"


def spam_of_control_alert(person):

    if person["spam_of_control"] > 15:
        return "Crítico"
    elif person["spam_of_control"] > 10:
        return "Moderado"

    return "Sem alerta"


@svc.api(
    input=JSON(), output=JSON(),
)
async def classify(input_series):
    results = []
    for person in input_series:
        raise_alert_result = raise_alert(person)
        results.append({
            "id": person["id"],
            "extra_hour_alert": extra_hour_alert(person),
            "raise_alert": raise_alert_result,
            "turnover_alert": turnover_alert(person, raise_alert_result),
            "spam_of_control_alert": spam_of_control_alert(person)
        })
    return results

'''
[
      {
         "id":1,
         "date_of_hire":"2022-03-03",
         "company_time_in_days": 900,
         "spam_of_control":3,
         "manager_climate_factors":[
            
         ],
         "number_of_raises_without_adjusts":4,
         "evaluation":null,
         "last_six_months_extra_hours":[
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/676/?format=json",
               "created":"2023-03-08T19:36:08.590677Z",
               "modified":"2023-03-08T19:36:08.590727Z",
               "date":"2023-03-08",
               "hours":790.0,
               "amount_currency":"BRL",
               "amount":4452.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/1/?format=json",
               "created":"2023-03-03T14:57:25.854784Z",
               "modified":"2023-03-03T14:57:25.854830Z",
               "date":"2023-02-26",
               "hours":1.099048055748295e16,
               "amount_currency":"BRL",
               "amount":79.5071,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/677/?format=json",
               "created":"2023-03-08T19:36:08.590848Z",
               "modified":"2023-03-08T19:36:08.590866Z",
               "date":"2023-02-08",
               "hours":391.0,
               "amount_currency":"BRL",
               "amount":2091.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/678/?format=json",
               "created":"2023-03-08T19:36:08.590928Z",
               "modified":"2023-03-08T19:36:08.590942Z",
               "date":"2023-01-08",
               "hours":932.0,
               "amount_currency":"BRL",
               "amount":3818.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/679/?format=json",
               "created":"2023-03-08T19:36:08.590997Z",
               "modified":"2023-03-08T19:36:08.591012Z",
               "date":"2022-12-08",
               "hours":238.0,
               "amount_currency":"BRL",
               "amount":3988.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/680/?format=json",
               "created":"2023-03-08T19:36:08.591067Z",
               "modified":"2023-03-08T19:36:08.591082Z",
               "date":"2022-11-08",
               "hours":414.0,
               "amount_currency":"BRL",
               "amount":4384.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/681/?format=json",
               "created":"2023-03-08T19:36:08.591135Z",
               "modified":"2023-03-08T19:36:08.591149Z",
               "date":"2022-10-08",
               "hours":37.0,
               "amount_currency":"BRL",
               "amount":670.0,
               "person":"http://localhost:5050/client1/api/v1/people/1/?format=json"
            }
         ],
         "last_raise_date_without_adjusts":"2023-03-08"
      },
      {
         "id":2,
         "date_of_hire":"2022-03-03",
         "spam_of_control":3,
         "company_time_in_days": 900,
         "manager_climate_factors":[
            {
               "factor_name":"Categoria #1",
               "score":10.01,
               "date":"2023-03-03",
               "company_average":77.92
            },
            {
               "factor_name":"Categoria #2",
               "score":44.89,
               "date":"2023-03-03",
               "company_average":139.62
            },
            {
               "factor_name":"Categoria #3",
               "score":77.66,
               "date":"2023-03-03",
               "company_average":109.23
            },
            {
               "factor_name":"Categoria #4",
               "score":219.29,
               "date":"2023-03-03",
               "company_average":174.31
            },
            {
               "factor_name":"Categoria #5",
               "score":169.33,
               "date":"2023-03-03",
               "company_average":396.08
            }
         ],
         "number_of_raises_without_adjusts":2,
         "evaluation":null,
         "last_six_months_extra_hours":[
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/2/?format=json",
               "created":"2023-03-03T14:57:25.860767Z",
               "modified":"2023-03-03T14:57:25.860809Z",
               "date":"2023-06-05",
               "hours":3.737144912530226e16,
               "amount_currency":"BRL",
               "amount":82.5268,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/688/?format=json",
               "created":"2023-03-08T19:36:08.591598Z",
               "modified":"2023-03-08T19:36:08.591612Z",
               "date":"2023-03-08",
               "hours":466.0,
               "amount_currency":"BRL",
               "amount":4474.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/689/?format=json",
               "created":"2023-03-08T19:36:08.591665Z",
               "modified":"2023-03-08T19:36:08.591679Z",
               "date":"2023-02-08",
               "hours":17.0,
               "amount_currency":"BRL",
               "amount":4680.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/690/?format=json",
               "created":"2023-03-08T19:36:08.591731Z",
               "modified":"2023-03-08T19:36:08.591745Z",
               "date":"2023-01-08",
               "hours":418.0,
               "amount_currency":"BRL",
               "amount":3770.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/691/?format=json",
               "created":"2023-03-08T19:36:08.591797Z",
               "modified":"2023-03-08T19:36:08.591810Z",
               "date":"2022-12-08",
               "hours":832.0,
               "amount_currency":"BRL",
               "amount":1349.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/692/?format=json",
               "created":"2023-03-08T19:36:08.591862Z",
               "modified":"2023-03-08T19:36:08.591875Z",
               "date":"2022-11-08",
               "hours":183.0,
               "amount_currency":"BRL",
               "amount":4395.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            },
            {
               "api_url":"http://localhost:5050/client1/api/v1/extra_hours/693/?format=json",
               "created":"2023-03-08T19:36:08.591928Z",
               "modified":"2023-03-08T19:36:08.591942Z",
               "date":"2022-10-08",
               "hours":655.0,
               "amount_currency":"BRL",
               "amount":4240.0,
               "person":"http://localhost:5050/client1/api/v1/people/2/?format=json"
            }
         ],
         "last_raise_date_without_adjusts":"2022-06-08"
      }]
'''
