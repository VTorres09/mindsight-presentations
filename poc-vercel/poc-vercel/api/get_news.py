from http.server import BaseHTTPRequestHandler
from urllib.request import urlopen
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen
from cowpy import cow


class handler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        client = urlopen("https://news.google.com/news/rss")
        page = client.read()
        client.close()
        souped = soup(page, "xml")
        news_list = souped.findAll("item")
        result = []
        for news in news_list:
            data = {}
            data['title'] = news.title.text
            data['date'] = news.pubDate.text
            result.append(data)
        self.wfile.write(str(result).encode())
        return
